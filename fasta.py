# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 12:28:40 2017
 
@author: Migue 
"""

def Fasta(fichero):
    identificador=''
    secuencia=''
    
    palabras=[]
    
    for line in open(fichero):
        if line[0]=='>':
            if identificador!='':
                palabras.append([identificador,secuencia])
            line=line.split("\n")
            identificador=line[0].rstrip()
            secuencia=''
        else:
            secuencia=secuencia+line.rstrip()
    palabras.append([identificador,secuencia])
            
    return palabras