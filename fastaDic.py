# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 13:18:36 2017
   
@author: Migue
"""


def FastaDic(fichero):
    identificador=''
    secuencia=''
    
    identificadores=[]
    secuencias=[]
    
    with open(fichero) as inputfile:
        for line in inputfile:
            if line[0]=='>':
                if identificador!='':
                    identificadores.append(identificador)
                    secuencias.append(secuencia)
                line=line.split("\n")
                identificador=line[0].rstrip()
                secuencia=''
            
            else:
                secuencia=secuencia+line.rstrip()
                
        dictionary = dict(zip(identificadores,secuencias))
        print(dictionary)